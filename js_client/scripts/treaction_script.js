/**
 * @Author Aki
 * @internal
 * Main configuration for client page
 *Input:takes Settings and token value from files
 *Functions:
 *      1.Generate Form todo:remove it use templates from backend
 *      2.Send the lead data to AIO
 *      3.Send data to matomo
 *      4.Cookie information processing and saving
 *      5.todo:Splittest
 *      6.todo:page flow
 *      7.todo:client tracking b.z url,IP etc.. tracking information feature move to backend
 */

/*
@internal - 'use strict' before minify the file TODO: all js to be refactored with mode strict => minify & obfuscate
@internal-taking settings from the settings file  todo: take this values form the html file
*/
let settingsValue = function () {
    let jsonTemp = null;
    $.ajax({
        'async': false,
        'url': 'settings.txt',
        'success': function (data) {
            jsonTemp = data;
        }
    });
    return jsonTemp;
}();

//@internal- global variables
let basicConfig;
let apiInOneSystem;
let basePayLoad = [];
let mappingData;
let activeProcesshook = false;
let processhookStatus;
let htmlFields;


//@internal-extract settings for multiple use cases
if (settingsValue) {
    basicConfig = JSON.parse(atob(settingsValue));
    apiInOneSystem = basicConfig['additionalSettings']['AIOUrl'];

    if (basicConfig) {
        $.each(basicConfig, function (key, value) {
            switch (key) {
                case 'mapping':
                    mappingData = JSON.parse(atob(value['mappingTable']))['contact'];
                    break;
                case 'additionalSettings':
                    // Map additional settings required to create Contact.
                    basePayLoad['apikey'] = value['apiKey'];
                    basePayLoad['objectregister_id'] = value['objectregister_id'];
                    basePayLoad['accountNumber'] = value['accountNumber'];
                    basePayLoad['uuid'] = value['uuid'];
                    break;
                case 'processhooks':
                    //make buttons for each processhook and create events
                    if(value && value !== '{}'){
                        activeProcesshook = true;
                    }
                    break;
                case 'processhookStatus':
                    processhookStatus = value;
                    break;
                default:
                // code block
            }
        });
    }
}

/*
    @internal- This is the main function that executes on load
    @internal - on page load functions
    @internal each code snippet has some functionality for the client
 */
$(document).ready(function () {

    //@internal- Generate html and append it
    if (mappingData) {
        generateForm(mappingData);
    }

    //@internal- append policy urls
    if (basePayLoad['accountNumber']) {
        appendPolicyUrls(basePayLoad);
    }

    //@internal- generate buttons
    if (activeProcesshook) {
        appendButtons(processhookStatus);
    }else{
        //@internal append workflow for send button (default button)
        let workflowId;
        if(processhookStatus && processhookStatus['index.html']['Submit']['workflowId']){
            workflowId = processhookStatus['index.html']['Submit']['workflowId'];
        }
        $('#send').attr('workflowId', workflowId);
    }


    //@internal- send data to AIO
    $('.formButton').click(function (e) {
        //@internal - global const
        const form = $('.check-submit-form');
        let formData = form.serializeArray();
        checkDataForMissingFields(formData);
        $(".check-submit-form input:checkbox").each(function(){
            //check box fields not taken if not checked
            if(!this.checked){
                let additionCheckboxFields = {
                    'name':this.name,
                    'value': 'false'
                };
                formData.push(additionCheckboxFields);
            }
        });
        e.preventDefault();
        this.disabled = true;
        //@internal- management of data and
        let workFlowId = $(this).attr('workflowId');
        sendDataToAIO(formData, workFlowId);
        manageFlow();
    });
});


/**
 * @param {array} processhookStatus
 * @author AKi
 * @comment generates additional buttons with different works flows
 */
function appendButtons(processhookStatus) {
    const buttonsHtml = $('#buttons');
    $.each(processhookStatus, function (key, val) {
        if (key === "index.html") {
            $.each(val, function (index, value) {
                if (index !== "Submit") {
                    let button = '<button class="btn treaction-btn customButton formButton" id="' + index + '" ' +
                        'objectRegisterId="' + value['objectRegisterId'] + '" ' +
                        'workflowId="' + value['workflowId'] + '"  style="margin-top: 5px;' +
                        ' width: 94%; padding:20px;">' + index + ' &nbsp;</button>';
                    buttonsHtml.append(button);
                } else {
                    $('#send').attr('workflowId', value['workflowId']);
                }
            });
        }
    });
}


/**
 *
 * @return json.
 * @param basePayLoad
 * @param formData
 * @param mappingTable
 * @internal this function generate structure required for AIO
 */
function structureFormData(basePayLoad, formData, mappingTable) {
    $.each(mappingTable, function (index, element) {
        $.each(element, function (key, value) {
            let name = value['marketing-in-one'];
            let data = "";
            $.each(formData, function (a, b) {
                if (b.name === name) {
                    data = b.value;
                }
            });
            value[name] = data;
        });
    });
    return {
        "base": {
            "apikey": basePayLoad['apikey'],
            "account_number": basePayLoad['accountNumber'],
            "object_register_id": basePayLoad['objectregister_id'],
            "uuid": basePayLoad['uuid'],
            //"campaign_token": token_value[0]['value']
        },
        "contact": mappingTable
    };
}

/**
 * Unicode to ASCII (encode data to Base64)
 * @param {string} data
 * @return {string}
 */
function utoa(data) {
    return btoa(unescape(encodeURIComponent(data)));
}

/**
 * @internal get policy data for page and appends to the url of the respected variables
 * @param {array} baseData
 * @return {void}
 */
function appendPolicyUrls(baseData) {
    let data;
    //format the data in required form
    data = {
        "apikey": baseData['apikey'],
        "account_number": baseData['accountNumber'],
        "object_register_id": baseData['objectregister_id']
    };
    //encrypt the data and send to AIO
    let encryptedData = utoa(JSON.stringify(Object.assign({}, data)));
    let getPolicyDataFromAio = {
        'async': true,
        "url": apiInOneSystem + "/page/policy/get",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "text/plain"
        },
        "data": encryptedData,
    };
    //set the parameters in html
    $.ajax(getPolicyDataFromAio).done(function (response) {
        if (response['url_privacy_policy']) {
            $('#privacy_policy').attr('href', response['url_privacy_policy']);
            $('#cookiePrivacyPolicy').attr('href', response['url_privacy_policy']);
        }
        if (response['url_imprint']) {
            $('#imprint').attr('href', response['url_imprint']);
        }
        if (response['url_gtc']) {
            const agbTag = $('#agb');
            agbTag.attr('href', response['url_gtc']);
            agbTag.removeAttr('hidden');
        }
        if (response['website']) {
            const websiteTag = $('#website');
            websiteTag.attr('href', response['website']);
            websiteTag.text(response['website']);
        }
    });
}

/**
 * @internal- checks if url params exists and returns url params if exists.
 * @returns {boolean}
 */
function CheckUrlParams(){
    let url = window.location.href;
    if(url.indexOf('?') !== -1)
        return true;
    else if(url.indexOf('&') !== -1)
        return true;
    return false;
}

/**
 * @internal -checks if url params exists and returns url params if exists.
 * @returns {string}
 */
function generateUrlWithParams(url){
    let currentUrl = window.location.href;
    let parts = currentUrl.split('?', 2);
    let params = parts[1];
    return url + '?' + params;
}

/**
 * @internal -Generates form and appends it to index.html to div with id marketingInOneForm
 * @returns {void}
 * @param mioMappingTable.standard standard fields of MIO
 * @param mioMappingTable.custom custom fields of MIO
 */
function generateForm(mioMappingTable){
    //Merge all the MIO mapping fields
    htmlFields = [...mioMappingTable.standard,...mioMappingTable.custom];
    //Loop through fields and append the html
    $.each(htmlFields, function (key, value) {
        //add every field to form
        addFieldsToHtml(value);
    });
}

/**
 * @internal - Holds the html template and
 * @param {array} field
 * @returns {boolean}
 * @internal todo: reformat this method into smaller once , add regex validation
 */
function addFieldsToHtml(field){
    let fieldName = field['field_name'];
    let dataType =  field['datatype'];
    const form = $('#marketingInOneForm');
    //id,required
    if(fieldName === 'salutation' ){
        const htmlTemplate = $('#fieldName_salutation');
        form.append(htmlTemplate.html());
        return true;
    }
    if(dataType === 'Date' || dataType ==='DateTime' ){
        //holding html div in constants
        const htmlTemplate = $('#dataType_date');
        const htmlInputTag = $('#dataType_date :input');
        const htmlLabelTag = htmlTemplate.find('.label');
        let templateHtml = htmlTemplate.html();
        //adding the required attributes to template
        htmlInputTag.attr("name",fieldName);
        htmlInputTag.attr("id",fieldName);
        //Add the required properties
        if(field['required']) {// required fields will have * in place holder
            htmlInputTag.prop('required', true);
        }

        htmlLabelTag.attr("for",fieldName);
        htmlLabelTag.append(fieldName);

        if(dataType === 'Date'){
            htmlInputTag.attr("type","date");
        }else{
            htmlInputTag.attr("type","datetime-local");
        }

        //todo: add type ,label text,

        //Add the html to form
        form.append(htmlTemplate.html());
        //restore the template
        htmlTemplate.empty();
        htmlTemplate.html(templateHtml);
        return true;

    }else if(dataType === 'Boolean'){
        //holding html div in constants
        const htmlTemplate = $('#dataType_checkbox');
        const htmlInputTag = $('#dataType_checkbox :input');
        const htmlLabelTag = htmlTemplate.find('.label');
        let templateHtml = htmlTemplate.html();
        //adding the required attributes to template
        htmlInputTag.attr("name",fieldName);
        htmlInputTag.attr("id",fieldName);
        //Add the required properties
        if(field['required']) {// required fields will have * in place holder
            htmlInputTag.prop('required', true);
        }

        htmlLabelTag.attr("for",fieldName);
        htmlLabelTag.append(fieldName);
        //Add the html to form
        form.append(htmlTemplate.html());
        //restore the template
        htmlTemplate.empty();
        htmlTemplate.html(templateHtml);
        return true;

    }else{
        //holding html div in constants
        const htmlTemplate = $('#dataType_text');
        const htmlInputTag = $('#dataType_text :input');
        let templateHtml = htmlTemplate.html();
        //adding the required attributes to template
        htmlInputTag.attr("name",fieldName);
        htmlInputTag.attr("id",fieldName);
        //Add the required properties
        if(field['required']){// required fields will have * in place holder
            htmlInputTag.prop('required',true);
            htmlInputTag.attr('placeholder','*'+field['html_placeholder']);
        }else{
            htmlInputTag.attr('placeholder',field['html_placeholder']);
        }
        if(field['html_hidden_field']){
            htmlInputTag.prop('hidden',true);
        }
        //add type to the div
        switch (dataType) {
            case 'Email':
                htmlInputTag.attr('type', 'email');
                break;
            case 'Decimal':
            case 'Integer':
            case 'Phone':
                htmlInputTag.attr('type', 'number');
                break;
            case 'URL':
                htmlInputTag.attr('type', 'url');
                break;
            default:
                htmlInputTag.attr('type', 'text');
        }
        //Add the html to div
        form.append(htmlTemplate.html());
        //restore the template
        htmlTemplate.empty();
        htmlTemplate.html(templateHtml);
        return true;
    }
}

/**
 * @internal -Sends data to AIO.
 * @returns {boolean}
 */
function sendDataToAIO(data,workflowId){
    //structured form data with token
    let formDataWithToken = structureFormData(basePayLoad, data, mappingData);
    //if processhook is active
    formDataWithToken['base']['workflow_id'] = workflowId;
    let formJson = JSON.stringify(formDataWithToken);
    let formDataSent = utoa(formJson);
    let AioResponse = {
        "url": apiInOneSystem + "/v2.0/contact/create",
        "method": "POST",
        'async': true,
        'success': 'success',
        "timeout": 0,
        "headers": {
            "Content-Type": "text/plain"
        },
        "data": formDataSent
    };
    $.ajax(AioResponse).done(function () {
        return true;
    });
    return false;
}

/**
 * @internal -Sends data to AIO.
 * @returns {void}
 */
function manageFlow(){
    //@internal- divs as consts
    const formPage = $('#form');
    const successPage = $('#success');
    //internal- first hide the div - this solves the empty white page problem
    successPage.hide();
    successPage.load("success.html");
    setTimeout(function () {
        //@internal - this are body handling functions todo:make this a separate function with flow control
        formPage.html('');
        successPage.fadeIn('20');
        //@internal-redirect to client page
        setTimeout(function () {
            let redirectUrl = basicConfig['page_flow']['redirect_page'];
            //@internal - forward url params
            if (CheckUrlParams()) {
                redirectUrl = generateUrlWithParams(redirectUrl);
            }
            $(location).attr('href', redirectUrl);
        }, 5000);
    }, 500);
}

/**
 * @internal -Takes the form data and throws css error (small animation of the field) if field not filled.
 * @returns {void}
 */
function checkDataForMissingFields(formData){
    $.each(formData,function( index, element ){
        if(element['value'] === ""){
            let fieldName = element['name'];
            $.each(htmlFields,function(key, value ){
                if(value['field_name'] === fieldName){
                    //@internal- if the field is required and not filled just shake the fields with red colour
                    if(value['required']){
                        const htmlField = $("#"+fieldName);
                        htmlField.css({"border-color": "red", "outline": "none", "box-shadow": "none"});
                        htmlField.effect("shake");
                        throw new Error(fieldName+'is required but not filled');
                    }
                }
            });
        }
    });
    //check for user consent
    consentCheck();
}

/**
 * @internal -Checks for the checked consent in form
 * @returns {void}
 */
function consentCheck()
{
    const checkBox = $('input#consentCheckBox');
    let isConsentChecked = checkBox.prop('checked');
    if(!isConsentChecked){
        checkBox.css({"border-color": "red", "outline": "none", "box-shadow": "none"});
        checkBox.effect("shake");
        throw new Error('consentCheckBox is required but not filled');
    }
}







