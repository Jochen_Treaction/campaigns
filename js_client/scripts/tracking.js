//get the values for settings
//taking settings from the settings file

$(document).ready(function () {
    $("#cookieConsentOK").click(function () {
        //set the cookie
        // $.cookie('cookieConsent', true, {domain: thisdomain, expires: 365, path: '/', secure: true});
        treactionCookieSet();
        manageTracking();
        $('div#cookieConsent').css('display', 'none');
    });

    $("#cookieConsentNo").click(function () {
        $('div#cookieConsent').css('display', 'none');
    });
});

function treactionCookieSet() {
    let thisdomain = $(location).attr('hostname');
    let path = $(location).attr('pathname');
    document.cookie="name=TopLandingPage;Max-Age=2678400"+";Path="+path+";Secure=true"+';domain='+thisdomain+';SameSite=Strict';
}

function treactionCookieExists() { // used in index.html
    if (document.cookie.split(';').some((item) => item.trim().startsWith('name=TopLandingPage'))) {
        return true;
    }
    return false;
}


function getcookieValue() {
    const cookieValue = document.cookie
        .split('; ')
        .find(row => row.startsWith('name=TopLandingPage'))
        .split('=')[1];

    function showCookieValue() {
        const output = document.getElementById('cookie-value');
        output.textContent = '> ' + cookieValue;
    }

    showCookieValue();
}

function manageTracking() {
    //variables from mio
    let facebookPixel;
    let googleAnalyticsKey;
    let matomoAuthToken;
    let matomoUrl;

    let basicConfig = getBaseConfig();
    if (basicConfig) {
        facebookPixel = basicConfig['tracker']['facebook_pixel'];
        googleAnalyticsKey = basicConfig['tracker']['google_apikey'];
        matomoAuthToken = basicConfig['tracker']['matomo_auth_token'];
        matomoUrl = basicConfig['additionalSettings']['matomoUrl'];
    }
    //matomo tracking

    let matomoTrackingUrl = $(location).attr('href').replace('/form.html', '');
    let matomoQueryUrl = matomoUrl + "/?module=API&method=SitesManager.getSitesIdFromSiteUrl&url=" + matomoTrackingUrl + "&format=JSON&token_auth=" + matomoAuthToken;

    //get the id of the site
    let matomoAjax = {
        "url": matomoQueryUrl,
        "method": "GET",
        "dataType": "jsonp",
        "timeout": 0,
        "headers": {
            "Content-Type": "text/plain"
        },
    };

    //add id to site tracking
    $.ajax(matomoAjax).done(function (response) {
        let MatomoSiteId = response[0]['idsite'];

        let matomoImageHtml = '<img src=' + matomoUrl + '/matomo.php?idsite=' + MatomoSiteId + '&amp;rec=1 style="border:0" alt="" >';

        $(function () {
            setTimeout(() => {
                $("#MatomoPixel").append(matomoImageHtml);
            }, 20);
        });
    });

    //facebook tracking
    if (facebookPixel) {
        let fbUrl = 'https://www.facebook.com/tr?id="' + facebookPixel + ' "&ev=PageView&noscript=1';
        //accural script code starts hear
        /*Get the facebook pixel code from config.ini */
        !function (f, b, e, v, n, t, s) {
            if (f.fbq)
                return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)
                f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s);
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', facebookPixel);
        fbq('track', 'PageView');

        window.fbAsyncInit = function () {
            fb_event.forEach(function (e) {
                e = e.trim();
                // Purchace event needs currency and amount value
                if (e == "Purchase") {
                    fbq('track', e, {currency: fb_curreny, value: fb_value});
                } else {
                    fbq('track', e);
                }
            });
        };

        let HtmlFb = '<img height="1" width="1" style="display:none" src= "' + fbUrl + '">';
        // mapping this HTML!!!!!!!!!!!
        $(function () {
            setTimeout(() => {
                $("#fbUrl").append(HtmlFb);
            }, 20);
        });
    }

    //google tracking
    if (googleAnalyticsKey) {
        let s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "https://www.googletagmanager.com/gtag/js?id=" + googleAnalyticsKey;
        s.setAttribute('async', '');
        $("head").append(s);

        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', googleAnalyticsKey);
    }
}


/**
 * @return {array}
 * @internal Get the base settings of the page
 */
function getBaseConfig(){
    let settingsValue = function () {
        let jsonTemp = null;
        $.ajax({
            'async': false,
            'url': 'settings.txt',
            'success': function (data) {
                jsonTemp = data;
            }
        });
        return jsonTemp;
    }();
    return JSON.parse(atob(settingsValue));
}





