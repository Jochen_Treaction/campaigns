/**
 * @internal
 * Main configuration for client page
 *Input:takes Settings and token value from files
 *Functions:
 *      1.Generate Form todo:remove it use templates from backend
 *      2.Send the lead data to AIO
 * @Author Aki
 */

//@internal-taking settings from the settings file
let mappingTable;
let settingsUrl = 'settings.txt';
let settingsValue = function () {
    let jsonTemp = null;
    $.ajax({
        'async': false,
        'url': settingsUrl,
        'success': function (data) {
            jsonTemp = data;
        }
    });
    return jsonTemp;
}();

//@internal- global variables
let webhookSettings = JSON.parse(atob(settingsValue));
let activeProcesshook = false;
let processhookStatus;
let apiInOneSystem;
let mappingData;
let htmlFields;

//@internal - mapping data to global variables
if (webhookSettings) {
    let Mapping = webhookSettings['mapping'];
    apiInOneSystem = webhookSettings['additionalSettings']['AIOUrl'];
    if (webhookSettings['processhooks']) {
        activeProcesshook = true;
        processhookStatus = webhookSettings['processhookStatus'];
    }
    if (typeof (Mapping) === "string") {
        mappingData = JSON.parse(atob(Mapping), true)['contact'];
    } else {
        mappingData = JSON.parse(atob(Mapping['mappingTable']))['contact'];
    }
}

/*
@internal- This is the main function that executes on load
@internal - on page load functions
@internal each code snippet has some functionality for the client
*/
$(document).ready(function () {
    //@internal - Add heading to webpage
    addHeading();

    //@internal- Generate html and append it
    if (mappingData) {
        generateForm(mappingData);
    }

    //@internal- send data to AIO
    $('input#submitButton').click(function (e) {
        //@internal - global const
        const form = $('.check-submit-form');
        let formData = form.serializeArray();
        checkDataForMissingFields(formData);
        $(".check-submit-form input:checkbox").each(function(){
            //check box fields not taken if not checked
            if(!this.checked){
                let additionCheckboxFields = {
                    'name':this.name,
                    'value': 'false'
                };
                formData.push(additionCheckboxFields);
            }
        });
        e.preventDefault();
        this.disabled = true;
        $("input[type=submit]").val('Please wait Submitting your data');
        //@internal- management of data and
        let workFlowId = $(this).attr('workflowId');
        sendDataToAIO(formData, workFlowId);
    });
});


/**
 * @internal -Sends data to AIO.
 * @internal - handling of text while sending the data
 * @param {array}formData data collected from UI
 * @param workflowId
 * @returns {void}
 */
function sendDataToAIO(formData, workflowId) {

    //push additional values to form data
    let additionalSettings = webhookSettings['additionalSettings'];
    let structuredFormData = structureFormData(additionalSettings, formData, mappingData);
    if (activeProcesshook) {
        structuredFormData['base']['workflow_id'] = workflowId;
    }
    //const for input button handling
    const inputSubmitButton = $('input#submitButton');
    let formJson = JSON.stringify(structuredFormData);
    let formbtoa = utoa(formJson);
    $.ajax({
        url: apiInOneSystem + "/v2.0/webhook/receive",
        type: 'POST',
        async: false,
        timeout: 0,
        header: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
            'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
            'Access-Control-Max-Age': '86400'
        },
        data: formbtoa,
        success: function (data) {
            let response = (data);
            if (response) {

                inputSubmitButton.css('background-color', 'green');
                inputSubmitButton.val('Data submitted successfully');
            } else {
                inputSubmitButton.css('background-color', 'green');
                inputSubmitButton.val('Something went wrong');
            }
        }
    });
}

/**
 * @internal This methods generates html for form
 * @param formField single instance of a mapping field.
 * @return Html text.
 */
function generateHtml(formField) {
    let html;
    let requiredHtmlSymbol = "";
    let isHtmlRequired = false;
    let fieldName = formField['field_name'];
    let placeHolder = formField['html_placeholder'];
    if (formField['required']) {
        requiredHtmlSymbol = "*";
        isHtmlRequired = true;
    }
    //@internal only datetime local is supported in html
    let datatype = ((formField['datatype'] === 'DateTime') ? "datetime-local" : formField['datatype']);

    if (fieldName === 'salutation') {
        return `
                    <div class="form-row">
                      <div class="form-group col-md-12" style="width:100%;">
                        <select name= "` + fieldName + `" style="width:100%;" class="input-fields form-control" required>
                        <option value="None">Please Select</option>
                        <option value="Herr">Herr</option>
                        <option value="Frau">Frau</option>
                      </select>
                    </div>
                    </div>  
                    `;
    }
    if(datatype === 'Date' || datatype ==='datetime-local' ){
        html = `
                     <div class="form-row">
                       <div class="form-group col-md-12">
                       <label class="control-label" for="` + fieldName + `">` + placeHolder + `</label>
                       <input style="color:#000; ;background:transparent;" value="" 
                       class="btn-outline-dark input-fields form-control inline"
                        id="` + fieldName + `"
                        name="` + fieldName + `"
                        placeholder=" ` + placeHolder + `` + requiredHtmlSymbol + `"
                        type="` + datatype + `"
                        required = "` + isHtmlRequired + `">
                        </div>
                    </div>
                    `;

    }
    else if(datatype === 'Boolean') {
        html = `
                     <div class="form-row">
                       <div class="form-group col-md-12">
                       <input style="color:#000; ;background:transparent; "
                       class="btn-outline-dark input-fields input_dataType_checkbox"
                        id="` + fieldName + `"
                        name="` + fieldName + `"
                        type="checkbox"
                        value="true"
                        required = "` + isHtmlRequired + `">
                        <label class="form-check-label label" for="` + fieldName + `">` + placeHolder + `</label>
                        </div>
                    </div>
                    `;
    }
    else {
        html = `
                     <div class="form-row">
                       <div class="form-group col-md-12">
                       <input style="color:#000; ;background:transparent;" value="" 
                       class="btn-outline-dark input-fields form-control"
                        id="` + fieldName + `"
                        name="` + fieldName + `"
                        placeholder=" ` + placeHolder + `` + requiredHtmlSymbol + `"
                        type="` + datatype + `"
                        required = "` + isHtmlRequired + `">
                        </div>
                    </div>
                    `;
    }
    return html;
}

/**
 * Unicode to ASCII (encode data to Base64)
 * @param {string} data
 * @return {string}
 */
function utoa(data) {
    return btoa(unescape(encodeURIComponent(data)));
}


/**
 *
 * @return json.
 * @param basePayLoad
 * @param formData
 * @param mappingTable
 * @internal this function generate structure required for AIO
 * @internal This function also handles boolean data type WEB-5832
 */
function structureFormData(basePayLoad, formData, mappingTable) {

    $.each(mappingTable, function (index, element) {
        $.each(element, function (key, value) {
            let name = value['field_name'];
            let data = "";

            $.each(formData, function (a, b) {
                if (b.name === name) {
                    data = b.value;
                }
            });
            value[name] = data;
        });
    });
    return {
        "base": {
            "apikey": basePayLoad['apiKey'],
            "account_number": basePayLoad['accountNumber'],
            "object_register_id": basePayLoad['objectregister_id'],
            "uuid": basePayLoad['uuid'],
        },
        "contact": mappingTable
    };
}

/**
 * @internal -Generates form and appends it to index.html to div with id marketingInOneForm
 * @returns {void}
 */
function generateForm(mioMappingTable) {
    //html div for form
    const form = $('#marketingInOneForm');
    //Merge all the MIO mapping fields
    if(mioMappingTable.custom)
        htmlFields = [...mioMappingTable.standard, ...mioMappingTable.custom];
    else
        htmlFields = [...mioMappingTable.standard];
    //Loop through fields and append the html
    $.each(htmlFields, function (key, value) {
        //add every field to form
        let html = generateHtml(value);
        form.append(html);
    });
}


/**
 * @internal - Adding h1 tag to the webpage
 * @returns {void}
 */
function addHeading() {
    //@internal - Get the name of the webhook
    let campaignName = webhookSettings['additionalSettings']['CampaignName'];
    let HTML = "Please test your webhook '" + campaignName + "'";
    $('#pageName').append(HTML);
}


/**
 * @internal -Takes the form data and throws css error (small animation of the field) if field not filled.
 * @returns {void}
 */
function checkDataForMissingFields(formData){
    $.each(formData,function( index, element ){
        if(element['value'] === ""){
            let fieldName = element['name'];
            $.each(htmlFields,function(key, value ){
                if(value['field_name'] === fieldName){
                    //@internal- if the field is required and not filled just shake the fields with red colour
                    if(value['required']){
                        const htmlField = $("#"+fieldName);
                        htmlField.css({"border-color": "red", "outline": "none", "box-shadow": "none"});
                        htmlField.effect("shake");
                        throw new Error(fieldName+'is required but not filled');
                    }
                }
            });
        }
    });
}

